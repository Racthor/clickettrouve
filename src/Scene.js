import React from 'react';
import Score from './UI/Score.js';
import Erreurs from './UI/Erreurs.js';
import Essai from './UI/Essai.js';
import Background from './Assets/Background.js';
import SecretEtat from './SecretState.js';

import secretsList from './data/secrets.json'
import { isLabelWithInternallyDisabledControl } from '@testing-library/user-event/dist/utils';

class Scene extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            secrets: secretsList.map((elt) => {return new SecretEtat(elt)}),
            erreurs: [false, false, false],
            score: 0,
            secretsToString: '',
        }
    }

    reset() {
        this.setState ({
            secrets: secretsList.map((elt) => {return new SecretEtat(elt)}),
            erreurs: [false, false, false],
            score: 0,
            secretsToString: '',
        });
    }

    handleSubmit(value) {
        let change = {};
        const secretIndex = this.state.secrets.findIndex((elt) => elt.isName(value));
        if (secretIndex !== -1) {
            this.state.secrets[secretIndex].hasBeenFind();
            const newScore = this.state.score + 1;
            const toString = this.state.secretsToString + '|' + this.state.secrets[secretIndex].nomSecret;
            change = {
                score: newScore,
                secretsToString: toString,
            };
        } else {
            const oldErreurs = this.state.erreurs.slice();
            oldErreurs[oldErreurs.findIndex((elt) => !elt)] = true;
            change = {
                erreurs: oldErreurs,
            };
        }
        this.setState(change);
    }

    isWin() {
        const score = this.state.score;
        const nbSecret = this.state.secrets.length;
        const erreurs = this.state.erreurs;
        let confirm = false;
        if(score === nbSecret) {
            confirm = window.confirm("C'est gagné bravo ! Vous pouvez recommencer.");
        } else if(erreurs[erreurs.length - 1]){
            confirm = window.confirm("Trop d'erreurs, dommage. Allez on recommence !")
        }
        return confirm;
    }

    handleClick() {
        this.reset();
    }

    render() {
        const game = this.isWin()
            ? <button onClick={() => this.handleClick()}>Recommencer</button>
            : <Essai secrets={this.state.secrets} onSubmit={(v) => {this.handleSubmit(v)}} /> ;
        return (
            <div>
                <Score score={this.state.score} totalSecrets={this.state.secrets.length} secretsTrouve={this.state.secretsToString} />
                <Erreurs erreurs={this.state.erreurs} />
                {game}
                <Background />
            </div>
        );
    }
}

export default Scene;