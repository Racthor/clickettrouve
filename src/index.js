import ReactDOM from 'react-dom';
// import './index.css';
import Scene from './Scene.js';

ReactDOM.render(
    <Scene />,
    document.getElementById('root')
);