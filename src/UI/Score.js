function Score(props) {
    return (
        <div>
            <p>SCORE : {props.score} /{props.totalSecrets} {props.secretsTrouve}</p>
        </div>
    );
}

export default Score;