import React from 'react';

function UnitError(props) {
    return <li /*key={props.index}*/ style={{display: 'inline'}}>X</li>;
}

function Erreurs(props) {
    const errorList = Array(3);
    //console.log(errorList);
    for (const[i, b] of props.erreurs.entries()) {        
        if (b) {
            errorList.push(<UnitError key={i}/>);
        }
    }
    return (
        <div>
            Erreurs :
            <ul style={{display: 'inline'}}>
                {errorList}
            </ul>
        </div>
    );
}

export default Erreurs;