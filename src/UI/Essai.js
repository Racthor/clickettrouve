import React from 'react';

class Essai extends React.Component{
    constructor(props) {
      super(props);
      this.state = {value: ''};
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event, fun) {
      event.preventDefault();
      fun(this.state.value);
      this.setState({value: ''});
    }

    render() {
        return (
            <form onSubmit={(e) => this.handleSubmit(e, this.props.onSubmit)}>
              <label>
                Secret ?
                <input type="text" value={this.state.value} onChange={this.handleChange} />        </label>
              <input type="submit" value="OK" />
            </form>
        );
    }
}

export default Essai;