class SecretState {
    constructor(nomSecret) {
        this.nomSecret = nomSecret;
        this.etat = false;
    }

    setEtat(etat) {
        this.etat = etat;
    }

    isName(value) {
        return value === this.nomSecret;
    }

    hasBeenFind() {
        this.setEtat(true);
    }
}

export default SecretState;